# Pokémon Patched Crystal [Run Edition]

This is a disassembly of Pokémon Patched crystal using pokecrystal as a base

Below you gonna find all the info you need:

[**Project FAQ**](FAQ.txt)

[**Changelog**](ReadMe.txt)

[**Encounters and Evolutions**](Encounters&Evolutions.txt)

[**Want to try this out? You can get the rom, click here!**][release]

To set up the repository, see [**INSTALL.md**](INSTALL.md).

## Other cool things not by me

* Disassembly of [**Pokémon Red/Blue**][pokered]
* irc: [**freenode#pret**][irc]

[pokered]: https://github.com/iimarckus/pokered
[Pret irc]: https://kiwiirc.com/client/irc.freenode.net/?#pret
[release]: https://github.com/JySzE/patched-crystal-run-edition/releases
